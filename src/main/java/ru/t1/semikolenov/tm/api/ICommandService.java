package ru.t1.semikolenov.tm.api;

import ru.t1.semikolenov.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
