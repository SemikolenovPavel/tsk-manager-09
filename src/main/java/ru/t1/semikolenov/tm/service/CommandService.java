package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.ICommandRepository;
import ru.t1.semikolenov.tm.api.ICommandService;
import ru.t1.semikolenov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
